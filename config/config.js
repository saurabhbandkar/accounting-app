const dotenv = require("dotenv");
dotenv.config();

module.exports = {
  development: {
    mongoURI:
      "mongodb+srv://secondary_user:o735NzgLqvtz93Hx@cluster0.wpvoj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    TOKEN_SECRET: process.env.TOKEN_SECRET,
    REFRESH_TOKEN_SECRET: process.env.REFRESH_TOKEN_SECRET,
    API_KEY: process.env.API_KEY,
  },
  production: {
    mongoURI:
      "mongodb+srv://secondary_user:o735NzgLqvtz93Hx@cluster0.wpvoj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    TOKEN_SECRET: process.env.TOKEN_SECRET,
    REFRESH_TOKEN_SECRET: process.env.REFRESH_TOKEN_SECRET,
  },
};
