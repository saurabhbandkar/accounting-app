import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import AppNavbar from "./components/AppNavbar";
import HomePage from "./components/HomePage";
import AppLayout from "./components/AppLayout";
import ProtectedRoute from "./components/ProtectedRoute";
import { Provider } from "react-redux";
import store from "./store";
import { Container } from "reactstrap";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import PartyModal from "./components/PartyModal";
import DataEntryModal from "./components/DataEntryModal";
import PartyHome from "./components/PartyHome";
import Dashboard from "./components/Dashboard";

function App() {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <div className="App">
          <AppNavbar />
          <Container>
            <Switch>
              {/* <Route exact path="/admin" component={UserDetails} /> */}
              <Route exact path="/" component={HomePage} />
              {/* <Route exact path="/signup" component={SignUp} /> */}
              {/* <ProtectedRoute exact path="/app" component={AppLayout} />
              <ProtectedRoute exact path="/party" component={PartyHome} />
              <ProtectedRoute exact path="/addparty" component={PartyModal} /> */}
              {/* <ProtectedRoute exact path="/logout" component={HomePage} /> */}
              <ProtectedRoute exact path="/dashboard" component={Dashboard} />
              {/* <ProtectedRoute
                exact
                path="/adddata"
                component={DataEntryModal}
              /> */}
              <Route path="*" component={() => "404 not found"} />
            </Switch>
          </Container>
        </div>
      </Provider>
    </BrowserRouter>
  );
}

export default App;
