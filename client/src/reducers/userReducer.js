import {
  GET_USERS,
  ADD_USER,
  DELETE_USER,
  DATA_LOADING,
} from "../actions/types";
const initialState = {
  users: [
    // {
    //   id: uuid(),
    //   name: "Saurabh",
    //   email: "saurabhbandkar@gmail.com",
    //   contact: "1234567890",
    //   role: "user",
    // },
    // {
    //   id: uuid(),
    //   name: "Bumrah",
    //   email: "Bumrah@gmail.com",
    //   contact: "1234567890",
    //   role: "user",
    // },
    // {
    //   id: uuid(),
    //   name: "Sachin",
    //   email: "Sachin@gmail.com",
    //   contact: "1234567890",
    //   role: "user",
    // },
    // {
    //   id: uuid(),
    //   name: "Rohit",
    //   email: "Rohit@gmail.com",
    //   contact: "1234567890",
    //   role: "user",
    // },
    // {
    //   id: uuid(),
    //   name: "Boult",
    //   email: "Boult@gmail.com",
    //   contact: "1234567890",
    //   role: "user",
    // },
  ],
  isLoading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
        isLoading: false,
      };
    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter((user) => user._id !== action.payload),
      };
    case ADD_USER:
      return {
        ...state,
        users: [action.payload, ...state.users],
      };
    case DATA_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    default:
      return state;
  }
}
