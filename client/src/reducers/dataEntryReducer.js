import {
  ADD_DATA_ENTRY_DETAIL,
  GET_DATA_ENTRY_DETAILS,
  DELETE_DATA_ENTRY_DETAIL,
  UPDATE_DATA_ENTRY_DETAIL,
  DATA_ENTRY_LOADING,
} from "../actions/types";
const initialState = {
  data: [],
  isLoading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_DATA_ENTRY_DETAILS:
      return {
        ...state,
        data: action.payload,
        isLoading: false,
      };
    case DELETE_DATA_ENTRY_DETAIL:
      return {
        ...state,
        data: state.data.filter(
          (dataEntry) => dataEntry._id !== action.payload
        ),
      };
    case ADD_DATA_ENTRY_DETAIL:
      return {
        ...state,
        data: [action.payload, ...state.data],
      };
    case UPDATE_DATA_ENTRY_DETAIL:
      return {
        ...state,
        party: [action.payload, ...state.party],
      };
    case DATA_ENTRY_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    default:
      return state;
  }
}
