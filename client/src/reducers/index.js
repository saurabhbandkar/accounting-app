import { combineReducers } from "redux";
import userReducer from "./userReducer";
import partyReducer from "./partyReducer";
import dataEntryReducer from "./dataEntryReducer";
export default combineReducers({
  user: userReducer,
  partyDetail: partyReducer,
  dataEntryDetail: dataEntryReducer,
});
