import {
  GET_PARTY_DETAILS,
  ADD_PARTY_DETAIL,
  DELETE_PARTY_DETAIL,
  PARTY_DATA_LOADING,
  UPDATE_PARTY_DETAIL,
} from "../actions/types";
const initialState = {
  party: [],
  isLoading: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PARTY_DETAILS:
      return {
        ...state,
        party: action.payload,
        isLoading: false,
      };
    case DELETE_PARTY_DETAIL:
      return {
        ...state,
        party: state.party.filter(
          (partyData) => partyData._id !== action.payload
        ),
      };
    case ADD_PARTY_DETAIL:
      return {
        ...state,
        party: [action.payload, ...state.party],
      };
    case UPDATE_PARTY_DETAIL:
      return {
        ...state,
        party: [action.payload, ...state.party],
      };
    case PARTY_DATA_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    default:
      return state;
  }
}
