export const GET_USERS = "GET_USERS";
export const ADD_USER = "ADD_USER";
export const DELETE_USER = "DELETE_USER";
export const DATA_LOADING = "DATA_LOADING";
export const GET_TEST_DETAILS = "GET_TEST_DETAILS";
export const ADD_TEST_DETAIL = "ADD_TEST_DETAIL";
export const DELETE_TEST_DETAIL = "DELETE_TEST_DETAIL";
export const TEST_DATA_LOADING = "TEST_DATA_LOADING";
export const GET_DATA_ENTRY_DETAILS = "GET_DATA_ENTRY_DETAILS";
export const ADD_DATA_ENTRY_DETAIL = "ADD_DATA_ENTRY_DETAIL";
export const DELETE_DATA_ENTRY_DETAIL = "DELETE_DATA_ENTRY_DETAIL";
export const UPDATE_DATA_ENTRY_DETAIL = "UPDATE_DATA_ENTRY_DETAIL";
export const DATA_ENTRY_LOADING = "DATA_ENTRY_LOADING";
export const GET_PARTY_DETAILS = "GET_PARTY_DETAILS";
export const ADD_PARTY_DETAIL = "ADD_PARTY_DETAIL";
export const DELETE_PARTY_DETAIL = "DELETE_PARTY_DETAIL";
export const UPDATE_PARTY_DETAIL = "UPDATE_PARTY_DETAIL";
export const PARTY_DATA_LOADING = "PARTY_DATA_LOADING";
