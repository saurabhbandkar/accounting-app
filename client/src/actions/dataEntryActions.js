import {
  GET_DATA_ENTRY_DETAILS,
  ADD_DATA_ENTRY_DETAIL,
  DELETE_DATA_ENTRY_DETAIL,
  UPDATE_DATA_ENTRY_DETAIL,
  DATA_ENTRY_LOADING,
} from "./types";
import axios from "axios";

export const getDataEntryDetails = () => async (dispatch) => {
  dispatch(setDataEntryDetailsLoading());
  await axios.get("api/dataentry").then((res) => {
    dispatch({
      type: GET_DATA_ENTRY_DETAILS,
      payload: res.data,
    });
  });
};

export const addDataEntryDetails = (test) => (dispatch) => {
  axios.post("api/dataentry", test).then((res) =>
    dispatch({
      type: ADD_DATA_ENTRY_DETAIL,
      payload: res.data,
    })
  );
};

export const deleteDataEntryDetails = (id) => (dispatch) => {
  axios.delete(`api/dataentry/${id}`).then((res) => {
    dispatch({ type: DELETE_DATA_ENTRY_DETAIL, payload: id });
  });
};

export const updateDataEntryDetails = (id) => (dispatch) => {
  axios.post(`/api/dataentry/update/${id}`).then((res) => {
    dispatch({ type: UPDATE_DATA_ENTRY_DETAIL, payload: id });
  });
};

export const setDataEntryDetailsLoading = () => {
  return {
    type: DATA_ENTRY_LOADING,
  };
};
