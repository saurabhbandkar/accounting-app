import {
  GET_PARTY_DETAILS,
  ADD_PARTY_DETAIL,
  DELETE_PARTY_DETAIL,
  UPDATE_PARTY_DETAIL,
  PARTY_DATA_LOADING,
} from "./types";
import axios from "axios";

export const getPartyDetails = () => async (dispatch) => {
  dispatch(setPartyDetailsLoading());
  await axios.get("api/party").then((res) => {
    dispatch({
      type: GET_PARTY_DETAILS,
      payload: res.data,
    });
  });
};

export const addPartyDetails = (data) => (dispatch) => {
  axios.post("api/party", data).then((res) =>
    dispatch({
      type: ADD_PARTY_DETAIL,
      payload: res.data,
    })
  );
};

export const deletePartyDetails = (id) => (dispatch) => {
  axios.delete(`api/party/${id}`).then((res) => {
    dispatch({ type: DELETE_PARTY_DETAIL, payload: id });
  });
};

export const updatePartyDetails =
  ({ id, data }) =>
  (dispatch) => {
    axios.post(`/api/party/update/${id}`, data).then((res) => {
      dispatch({ type: UPDATE_PARTY_DETAIL, payload: res });
    });
  };

export const setPartyDetailsLoading = () => {
  return {
    type: PARTY_DATA_LOADING,
  };
};
