import { GET_USERS, ADD_USER, DELETE_USER, DATA_LOADING } from "./types";
import axios from "axios";

export const getUsers = () => async (dispatch) => {
  dispatch(setDataLoading());
  await axios.get("api/users").then((res) => {
    dispatch({
      type: GET_USERS,
      payload: res.data,
    });
  });
};
export const addUser = (user) => (dispatch) => {
  axios.post("api/users", user).then((res) =>
    dispatch({
      type: ADD_USER,
      payload: res.data,
    })
  );
};

export const deleteUser = (id) => (dispatch) => {
  axios.delete(`api/users/${id}`).then((res) => {
    dispatch({ type: DELETE_USER, payload: id });
  });
};

export const setDataLoading = () => {
  return {
    type: DATA_LOADING,
  };
};
