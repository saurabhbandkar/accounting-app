import React, { Component } from "react";
import { Container, ListGroup, ListGroupItem, Button } from "reactstrap";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { connect } from "react-redux";
import { getTestDetails, deleteTestDetail } from "../actions/testActions";
import { getUsers } from "../actions/userActions";
import PropTypes from "prop-types";
import TestingModal from "./TestingModal";
import Accordion from "./common/Accordion";

class UserDetails extends Component {
  componentDidMount() {
    this.props.getUsers();
    this.props.getTestDetails();
  }
  onDeleteClick = (id) => {
    this.props.deleteTestDetail(id);
  };
  render() {
    const { users } = this.props.user;
    const { testDetails } = this.props.testDetail;
    return (
      <Container>
        {/* <Button
          color="dark"
          style={{ marginBottom: "2rem" }}
          onClick={() => {
            const name = prompt("Enter name");
            if (name) {
              this.setState((state) => ({
                users: [
                  ...state.users,
                  {
                    id: uuid(),
                    name,
                    email: "surya@gmail.com",
                    contact: "1234567890",
                    role: "user",
                  },
                ],
              }));
            }
          }}
        >
          Add User
        </Button> */}
        <TestingModal />
        {/* <ListGroup>
          <TransitionGroup className="user-details">
            {users.map(
              ({ _id, name, email, contact, role, hasSubscribed = false }) => (
                <CSSTransition key={_id} timeout={500} className="fadeEffect">
                  <ListGroupItem>
                    <Button
                      classNames="remove-btn"
                      color="danger"
                      size="sm"
                      onClick={this.onDeleteClick.bind(this, _id)}
                    >
                      &times;
                    </Button>
                    {name}, {email}, {contact}, {role}, {hasSubscribed}
                  </ListGroupItem>
                </CSSTransition>
              )
            )}
          </TransitionGroup>
        </ListGroup> */}
        <Accordion allowMultipleOpen>
          {users.map(
            ({
              _id: userObjectId,
              name,
              email,
              contact,
              role,
              hasSubscribed = false,
            }) => (
              <div
                label={`Name=${name} Email=${email}`}
                key={userObjectId}
                isOpen
              >
                {testDetails
                  .filter((test) => {
                    return test.userid === userObjectId;
                  })
                  .map(
                    ({ _id: testId, testName, marksObtained, totalMarks }) => (
                      <div
                        style={{
                          border: "2px solid green",
                          margin: "1rem 0",
                        }}
                      >
                        <Button
                          classNames="remove-btn"
                          color="danger"
                          size="sm"
                          onClick={this.onDeleteClick.bind(this, testId)}
                        >
                          &times;
                        </Button>
                        {`Test name= ${testName} Marks Obtained= ${marksObtained} Total Marks= ${totalMarks}`}
                      </div>
                    )
                  )}
              </div>
            )
          )}
        </Accordion>
      </Container>
    );
  }
}

UserDetails.propTypes = {
  getUsers: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  testDetail: state.testDetail,
});
export default connect(mapStateToProps, {
  getUsers,
  deleteTestDetail,
  getTestDetails,
})(UserDetails);

{
  /* <div label="Alligator Mississippiensis" isOpen>
            <p>
              <strong>Common Name:</strong> American Alligator
            </p>
            <p>
              <strong>Distribution:</strong> Texas to North Carolina, United
              States
            </p>
            <p>
              <strong>Endangered Status:</strong> Currently Not Endangered
            </p>
          </div>
          <div label="Alligator Sinensis">
            <p>
              <strong>Common Name:</strong> Chinese Alligator
            </p>
            <p>
              <strong>Distribution:</strong> Eastern China
            </p>
            <p>
              <strong>Endangered Status:</strong> Critically Endangered
            </p>
          </div> */
}
