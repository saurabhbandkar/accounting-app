import React, { Component } from "react";
import { Container, ListGroup, ListGroupItem, Button } from "reactstrap";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { connect } from "react-redux";
import {
  getPartyDetails,
  deletePartyDetails,
  updatePartyDetails,
} from "../actions/partyActions";
import {
  getDataEntryDetails,
  deleteDataEntryDetails,
  updateDataEntryDetails,
} from "../actions/dataEntryActions";
// import TestingModal from "./TestingModal";
import Accordion from "./common/Accordion";
import axios from "axios";

class PartyHome extends Component {
  componentDidMount() {
    this.props.getPartyDetails();
    this.props.getDataEntryDetails();
  }
  onDeleteClick = (id) => {
    this.props.deletePartyDetails(id);
  };
  onDeleteDataEntryClick = async (id) => {
    let dataResponse = await axios.get(`/api/dataentry/${id}`);
    let partyResponse = await axios.get(
      `/api/party/${dataResponse?.data[0]?.userId}`
    );
    let balance =
      Number(partyResponse?.data[0]?.balance ?? 0) -
      Number(dataResponse?.data[0]?.amount ?? 0);
    await axios.post(`/api/party/update/${dataResponse?.data[0]?.userId}`, {
      balance: balance,
    });
    this.props.deleteDataEntryDetails(id);
    window.location.reload();
  };
  render() {
    const { party } = this.props.partyDetail;
    const { data: dataEntry } = this.props.dataEntryDetail;
    return (
      <Container>
        <Accordion allowMultipleOpen>
          {party.map(({ _id, partyName, balance }) => (
            <div
              label={`Name=${partyName} Balance=${balance}`}
              key={_id}
              isOpen
            >
              {dataEntry &&
                dataEntry
                  .filter((data) => {
                    return data.userId.toString() === _id.toString();
                  })
                  .map(({ _id: dataId, amount, date }) => (
                    <div
                      style={{
                        border: "2px solid green",
                        margin: "1rem 0",
                      }}
                      key={dataId}
                    >
                      <Button
                        classNames="remove-btn"
                        color="danger"
                        size="sm"
                        onClick={this.onDeleteDataEntryClick.bind(this, dataId)}
                      >
                        &times;
                      </Button>
                      {`amount= ${amount} Date= ${date.split("T")[0]}`}
                    </div>
                  ))}
            </div>
          ))}
        </Accordion>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    partyDetail: state.partyDetail,
    dataEntryDetail: state.dataEntryDetail,
  };
};
export default connect(mapStateToProps, {
  getPartyDetails,
  deletePartyDetails,
  updatePartyDetails,
  getDataEntryDetails,
  deleteDataEntryDetails,
  updateDataEntryDetails,
})(PartyHome);
