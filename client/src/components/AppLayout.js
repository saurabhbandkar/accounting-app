import axios from "axios";
import React from "react";
import auth from "./auth";

export default function AppLayout(props) {
  return (
    <div>
      App Layout
      <button
        onClick={() => {
          auth.logout(() => props.history.push("/"));
        }}
      >
        Logout
      </button>
      <button>Subscribe</button>
    </div>
  );
}
