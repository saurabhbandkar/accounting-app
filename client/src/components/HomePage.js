import React from "react";
import auth from "./auth";
export default function HomePage(props) {
  auth.logout();
  return (
    <div>
      HomePage
      <button
        onClick={() => {
          auth.login("test", () => props.history.push("/dashboard"));
        }}
      >
        Login
      </button>
    </div>
  );
}
