import { connect } from "react-redux";
import { Component } from "react";
import PartyModal from "./PartyModal";
import DataEntryModal from "./DataEntryModal";
import PartyHome from "./PartyHome";
import {
  getPartyDetails,
  deletePartyDetails,
  updatePartyDetails,
} from "../actions/partyActions";
import {
  getDataEntryDetails,
  deleteDataEntryDetails,
  updateDataEntryDetails,
} from "../actions/dataEntryActions";

class Dashboard extends Component {
  componentDidMount = () => {
    this.props.getPartyDetails();
    this.props.getDataEntryDetails();
  };
  //   componentDidUpdate = (prevProps) => {
  //     if (
  //       prevProps.partyDetail.party != this.props.partyDetail.party ||
  //       prevProps.dataEntryDetail.data != this.props.dataEntryDetail.data
  //     ) {
  //       this.props.getDataEntryDetails();
  //       this.props.getPartyDetails();
  //     }
  //   };
  render() {
    return (
      <>
        <PartyHome />
        <PartyModal />
        <DataEntryModal />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    partyDetail: state.partyDetail,
    dataEntryDetail: state.dataEntryDetail,
  };
};

export default connect(mapStateToProps, {
  getPartyDetails,
  deletePartyDetails,
  updatePartyDetails,
  getDataEntryDetails,
  deleteDataEntryDetails,
  updateDataEntryDetails,
})(Dashboard);
