import React, { Component } from "react";
import { Container, ListGroup, ListGroupItem } from "reactstrap";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { connect } from "react-redux";
import { getTestDetails, deleteTestDetail } from "../actions/testActions";
import {
  getPartyDetails,
  deletePartyDetails,
  updatePartyDetails,
} from "../actions/partyActions";
import { getUsers } from "../actions/userActions";
import PropTypes from "prop-types";

class PartyData extends Component {
  state = {
    userId: "616cf12af4a5427193cf132d",
  };
  componentDidMount() {
    this.props.getPartyDetails();
    this.props.getUsers();
  }

  render() {
    const { partyDetails } = this.props.partyDetails;
    return (
      <Container>
        <ListGroup>
          <TransitionGroup className="user-details">
            {partyDetails
              .filter(({ userid }) => userid === this.state.userId)
              .map(({ _id, testName, marksObtained, totalMarks, date }) => (
                <CSSTransition key={_id} timeout={500} className="fadeEffect">
                  <ListGroupItem>
                    {testName}, {marksObtained}, {totalMarks},{" "}
                    {date.toString().split("T")[0]}
                  </ListGroupItem>
                </CSSTransition>
              ))}
          </TransitionGroup>
        </ListGroup>
      </Container>
    );
  }
}

PartyData.propTypes = {
  getPartyDetails: PropTypes.func.isRequired,
  party: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  partyDetails: state.partyDetails,
});
export default connect(mapStateToProps, {
  getTestDetails,
  deleteTestDetail,
  getUsers,
  getPartyDetails,
  deletePartyDetails,
  updatePartyDetails,
})(PartyData);
