import React, { useState } from "react";
import { Formik, Form } from "formik";
import TextInput from "./common/TextInput";
import axios from "axios";
import Loader from "./common/Loader";
import ErrorModal from "./common/ErrorModal";

function Login() {
  const isEmpty = (value, falseStrings = [""]) =>
    value instanceof File
      ? false
      : typeof value === "undefined" ||
        value === null ||
        value !== value ||
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0) ||
        (typeof value === "string" && falseStrings.includes(value));

  const formInitialValues = {
    email: "",
    password: "",
  };
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [formData, setformData] = useState({});
  const [isError, setIsError] = useState(false);
  const [isEmailError, setIsEmailError] = useState(false);
  const [isPasswordError, setIsPasswordError] = useState(false);
  function formValidate(values) {
    let errors = {};
    if (isEmpty(values.password)) {
      errors.password = "Password is required";
    }
    if (isEmpty(values.email)) {
      errors.email = "User email is required";
    }
    return errors;
  }

  const handleSubmit = async (values) => {
    setIsLoading(true);
    setformData(values);
    try {
      const response = await axios.post("api/login", values);
      if (response?.status === 200) {
        setIsLoading(false);
        setIsFormSubmitted(true);
      }
    } catch (error) {
      setIsLoading(false);
      setIsPasswordError(true);
    }
  };
  const toggleEmailModal = () => {
    setIsEmailError(false);
  };
  const togglePasswordModal = () => {
    setIsPasswordError(false);
  };
  const renderView = ({
    values,
    handleSubmit,
    setFieldValue,
    setFieldTouched,
  }) => {
    if (isError) {
      return <p>Something went wrong</p>;
    }
    const { email = null, password = null } = values;
    return (
      <>
        <Form>
          <TextInput
            split
            labelTitle="email"
            labelFor="email"
            labelName="email"
            isMandatory
            placeholder="sherlock@gmail.com"
          />
          <TextInput
            split
            labelTitle="password"
            labelFor="password"
            labelName="password"
            isMandatory
            type="password"
          />
          <button type="submit" disabled={false}>
            Submit
          </button>
        </Form>
      </>
    );
  };
  return (
    <>
      {isLoading && <Loader />}
      <ErrorModal
        isModalOpen={isEmailError}
        errorMessage={"Email ID does not exists, please register"}
        toggleModal={toggleEmailModal}
      />
      <ErrorModal
        isModalOpen={isPasswordError}
        errorMessage={"Password is incorrect"}
        toggleModal={togglePasswordModal}
      />
      {!isFormSubmitted && (
        <div>
          <h1>Enter Login Credentials</h1>
          <Formik
            validate={formValidate}
            initialValues={formInitialValues}
            render={renderView}
            onSubmit={handleSubmit}
          />
        </div>
      )}
    </>
  );
}

export default Login;
