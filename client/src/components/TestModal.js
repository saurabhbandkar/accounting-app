import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { connect } from "react-redux";
import { addTestDetails } from "../actions/testActions";
import { v4 as uuid } from "uuid";
import CustomDropdown from "./common/CustomDropdown";

class TestModal extends Component {
  state = {
    isModalOpen: false,
    userid: "",
    totalMarks: "",
    marksObtained: "",
    testName: "",
  };
  toggle = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  };
  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  onSubmit = (event) => {
    event.preventDefault();
    const newTestDetail = {
      testName: this.state.testName,
      totalMarks: this.state.totalMarks,
      marksObtained: this.state.marksObtained,
      userid: this.state.userid,
    };
    this.props.addTestDetails(newTestDetail);
    this.toggle();
  };
  onValueChange = () => (props, values) => {
    console.log("dropdown change", values);
  };
  render() {
    return (
      <div>
        <Button
          color="dark"
          style={{ marginBottom: "2rem" }}
          onClick={this.toggle}
        >
          Add New Test
        </Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Add Test Details</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                {/* <CustomDropdown
                  items={["saurabh", "Surya"]}
                  //   { label: "saurabh", value: "test1" },
                  //   { label: "surya", value: "test2" },
                  // ]}
                /> */}
                {/* <DropDown
                  name="userid"
                  option={[
                    { label: "saurabh", value: "test1" },
                    { label: "surya", value: "test2" },
                  ]}
                  // handleCustom={this.onValueChange()}
                  isCustomHandle
                  split={false}
                  // customMenuList={props.customMenuList}
                  placeholder="Select candidate"
                  // addAdmin={props.addAdmin}
                  // addText={props.addText}
                  isClearable="false"
                  // onInputChange={onValueChange}
                  noOptionsMessageText="No users added"
                /> */}
                <Label for="userid">User Name</Label>
                <Input
                  type="text"
                  name="userid"
                  id="userid"
                  placeholder="Add user"
                  onChange={this.onChange}
                  isMandatory
                />
                <Label for="testName">Test Name</Label>
                <Input
                  type="text"
                  name="testName"
                  id="testName"
                  placeholder="Add test name"
                  onChange={this.onChange}
                  isMandatory
                />
                <Label for="marksObtained">Marks Obtained</Label>
                <Input
                  type="number"
                  name="marksObtained"
                  id="marksObtained"
                  placeholder="Add Mark sObtained"
                  isMandatory
                  onChange={this.onChange}
                />
                <Label for="totalMarks">Total Marks</Label>
                <Input
                  type="number"
                  name="totalMarks"
                  id="totalMarks"
                  placeholder="Add Total Marks"
                  isMandatory
                  onChange={this.onChange}
                />
                <Button color="dark" style={{ marginTop: "2rem" }} block>
                  Submit
                </Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  testDetail: state.testDetail,
});
export default connect(mapStateToProps, { addTestDetails })(TestModal);
