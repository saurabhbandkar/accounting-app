import { ErrorMessage, Field } from "formik";
import PropTypes from "prop-types";
import React from "react";
import renderHtml from "react-html-parser";
import Select, { components } from "react-select";
import AsyncSelect from "react-select/async";
import "./styles.scss";

const customStyles = {
  indicatorSeparator: () => ({
    width: 1,
  }),
  control: (base, state) => {
    return {
      ...base,
      boxShadow: "none",
      borderColor:
        state.isFocused || state.isHover || state.menuIsOpen
          ? "#979797"
          : base.borderColor,
    };
  },
};

const menuHeaderStyle = {
  padding: "8px 12px",
  backgroundColor: "white",
  fontSize: "14px",
  fontWeight: "600",
  color: "black",
};

const addMenuHeaderStyle = { ...menuHeaderStyle, cursor: "pointer" };

const MenuList = (props) => {
  return (
    <>
      <components.MenuList {...props}>
        {!props.isLoading &&
          props.selectProps.inputValue &&
          props.selectProps.inputValue.length > 1 && (
            <div style={menuHeaderStyle}>SEARCH RESULTS</div>
          )}
        <div className="mb-2" />
        {props.children}
      </components.MenuList>
      <div className="gray-line" />
      <div style={addMenuHeaderStyle} onClick={props.selectProps.addAdmin}>
        {props.selectProps.addText}
      </div>
    </>
  );
};

export const CustomReactSelect = (props) => {
  let {
    field: { name, onChange, onBlur, value },
    form: { setFieldValue, setFieldTouched },
    option,
    addAdmin,
    customClass,
  } = props;

  return (
    <div
      className={`select-group ${props.split ? "col-lg-5 p-0" : customClass}`}
    >
      <Select
        menuPlacement={props.menuPlacement || "auto"}
        isClearable={!!props.isClearable}
        styles={customStyles}
        //defaultValue={props.option[0]}
        isDisabled={props.disabled}
        options={props.option}
        components={props.customMenuList ? { MenuList } : {}}
        onChange={(e) => {
          setFieldTouched(name);
          setFieldValue(name, e);
          if (props.isCustomHandle) {
            let value = !!e ? e.value : e;
            props.handleCustom(value, props, e);
          }
        }}
        value={value}
        defaultValue={props.defaultValue}
        placeholder={props.placeholder}
        addAdmin={props.addAdmin}
        addText={props.addText}
        onInputChange={props.onInputChange}
      />
    </div>
  );
};

const DropDown = (props) => {
  return (
    <div className={props.split ? "row m-0 field-row-pb" : "form-group"}>
      <label
        htmlFor={props.labelFor}
        className={props.split ? "col-lg-5 mt-2 p-0" : ""}
      >
        {props.labelTitle}{" "}
        {props.isMandatory ? <span className="text-mandatory">*</span> : ""}
      </label>

      <Field
        className={props.split ? "form-control col-lg-5" : "form-control"}
        disabled={props.disabled}
        name={props.labelName}
        component={CustomReactSelect}
        option={props.option}
        handleCustom={props.handleCustom}
        isCustomHandle={props.isCustomHandle}
        split={props.split}
        customMenuList={props.customMenuList}
        placeholder={props.placeholder}
        addAdmin={props.addAdmin}
        addText={props.addText}
        isClearable={props.isClearable}
        menuPlacement={props.menuPlacement}
        customClass={props.customClass}
        onInputChange={props.onInputChange}
        noOptionsMessageText={props.noOptionsMessageText}
        minCharacter={props.minCharacter || 1}
        defaultValue={props.defaultValue || false}
      />
      {props.split && <div className={props.split && "col-lg-5"} />}
      {props.split && !props.notes && <div className={"col-lg-5"} />}
      {props.split && <div className={props.split && "col-lg-5"} />}
      <div className={props.split ? "error-text col-lg-5 pl-0" : "error-text"}>
        <ErrorMessage name={props.labelName} />
      </div>
    </div>
  );
};

DropDown.propTypes = {
  labelTitle: PropTypes.string.isRequired,
  labelName: PropTypes.string.isRequired,
  labelFor: PropTypes.string.isRequired,
  isMandatory: PropTypes.bool,
  isCustomHandle: PropTypes.bool,
  parseHTML: PropTypes.bool,
};

DropDown.defaultProps = {
  isMandatory: false,
  isCustomHandle: false,
  parseHTML: true, // Todo: this should be false by default.
};

export default DropDown;
