import React from "react";
import { Button, Modal, ModalHeader, ModalBody, Label } from "reactstrap";

function ErrorModal(props) {
  const { toggleModal, errorMessage, isModalOpen } = props;

  return (
    <div>
      <Modal isOpen={isModalOpen} toggle={toggleModal}>
        <ModalHeader>Success!!</ModalHeader>
        <ModalBody>
          <Label for="error">{errorMessage}</Label>
          <Button
            color="dark"
            style={{ marginLeft: "2rem" }}
            onClick={toggleModal}
          >
            Okay
          </Button>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default ErrorModal;
