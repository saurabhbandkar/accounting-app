import { Backdrop, CircularProgress, Typography } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.tooltip + 1,

    color: "#fff",
  },
}));

function Loader(props) {
  const classes = useStyles();

  return (
    <Backdrop className={classes.backdrop} open={true} onClick={() => {}}>
      <Typography
        variant="body1"
        align="center"
        display="block"
        style={{ position: "absolute", top: "40%" }}
      >
        {props.message ? props.message : "loading!!!"}
      </Typography>

      <CircularProgress color="inherit" />
    </Backdrop>
  );
}

export default Loader;
