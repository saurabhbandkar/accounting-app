import { ErrorMessage, Field } from "formik";
import PropTypes from "prop-types";
import React from "react";
import renderHtml from "react-html-parser";
import "./styles.scss";
const TextInput = (props) => {
  // let {errors, touched} = props;
  let labelColSpan = !!props.labelColSpan ? props.labelColSpan : "col-lg-5";
  let fieldPaddingBottom = !!props.fieldPaddingBottom
    ? props.fieldPaddingBottom
    : "field-row-pb";
  return (
    <div
      className={props.split ? `row m-0 ${fieldPaddingBottom}` : "form-group"}
    >
      <label
        htmlFor={props.labelFor}
        className={
          props.split ? `${labelColSpan} mt-2 p-0` : props.noLabel ? "mb-0" : ""
        }
      >
        {props.isTitleHtml ? renderHtml(props.labelTitle) : props.labelTitle}
        {props.isMandatory ? (
          <span className="text-mandatory">&nbsp;*</span>
        ) : (
          ""
        )}
      </label>
      {props.preUnit && (
        <span className={"p-10"} style={{ padding: "10px" }}>
          {renderHtml(props.preUnit)}{" "}
        </span>
      )}
      <Field
        disabled={props.disabled}
        style={{ height: 35 }}
        className={
          props.fieldClass
            ? props.fieldClass
            : props.split
            ? props.preUnit || props.unit
              ? "form-control col-lg-2"
              : "form-control col-lg-5"
            : "form-control"
        }
        type={props.type ? props.type : "text"}
        name={props.labelName}
        validate={props.validate}
        onWheel={(e) => {
          e.target.blur();
          e.preventDefault();
        }}
        // onScroll={(e) => {
        //   e.target.blur();
        //   e.preventDefault();
        // }}
        onKeyDown={(e) => {
          if (e.which === 38 || e.which === 40) {
            e.preventDefault();
          }
        }}
        {...props}
      />{" "}
      {props.isSupportingText && (
        <span
          className="form-support-text"
          style={
            props.supportingTextStyle
              ? props.supportingTextStyle
              : { color: "black" }
          }
        >
          {props.supportingTextValue}
        </span>
      )}
      {props.unit && (
        <span className={props.unitClass ? props.unitClass : "p-6"}>
          {" "}
          {props.unit}
        </span>
      )}
      {props.otherElement}
      {props.split && <div className={props.split ? "col-lg-5" : ""} />}
      {props.notes && (
        <div className={props.split ? "notes col-lg-5 pl-0" : "notes"}>
          {typeof props.notes === "string"
            ? renderHtml(props.notes)
            : props.notes}
        </div>
      )}
      {props.split && !props.notes && <div className={"col-lg-5"} />}
      {props.split && <div className={props.split ? `${labelColSpan}` : ""} />}
      <div className={props.split ? "error-text col-lg-5 pl-0" : "error-text"}>
        <ErrorMessage name={props.labelName} />
      </div>
    </div>
  );
};

TextInput.propTypes = {
  labelTitle: PropTypes.string.isRequired,
  labelName: PropTypes.string.isRequired,
  labelFor: PropTypes.string.isRequired,
  isMandatory: PropTypes.bool,
  isTitleHtml: PropTypes.bool,
};

TextInput.defaultProps = {
  isMandatory: false,
  isTitleHtml: false,
};

export default TextInput;
