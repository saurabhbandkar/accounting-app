import React, { Component } from "react";
import { Formik, Form } from "formik";
import TextInput from "./common/TextInput";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { connect } from "react-redux";
import { getPartyDetails, updatePartyDetails } from "../actions/partyActions";
import {
  addDataEntryDetails,
  getDataEntryDetails,
} from "../actions/dataEntryActions";
import { getUsers } from "../actions/userActions";
import DropDown from "./common/Dropdown";
import axios from "axios";
// import { v4 as uuid } from "uuid";

class PartyModal extends Component {
  state = {
    isModalOpen: false,
    formData: null,
    isLoading: false,
    isError: false,
    formInitialValues: {
      party_name: "",
      party_code: "",
      amount: "",
    },
  };
  componentDidMount() {
    this.props.getPartyDetails();
  }
  checkNumber = (number) => {
    let isNumber = false;
    if (number) {
      isNumber = isNaN(number) || parseInt(number) === 0;
    }
    return isNumber;
  };

  renderView =
    (partyDetail) =>
    ({ values, handleSubmit, setFieldValue, setFieldTouched }) => {
      const partyArray = partyDetail.party.map((partyData) => {
        return {
          value: partyData._id,
          label: partyData.partyName,
        };
      });
      const partyCodeArray = partyDetail.party.map((partyData) => {
        return {
          value: partyData._id,
          label: partyData.partyCode,
        };
      });
      if (this.state.isError) {
        return <p>Something went wrong</p>;
      }
      return (
        <>
          <Form>
            <DropDown
              labelTitle={"Select Party Name"}
              labelFor={"party_name"}
              labelName={"party_name"}
              isMandatory
              placeholder={"Select Party Name"}
              option={partyArray}
            />
            <DropDown
              labelTitle={"Select Party Code"}
              labelFor={"party_code"}
              labelName={"party_code"}
              isMandatory
              placeholder={"Select Party Code"}
              option={partyCodeArray}
            />
            <TextInput
              labelTitle={"Enter Amount"}
              labelFor={"amount"}
              labelName={"amount"}
              isMandatory
              placeholder={"Enter Amount"}
            />
            <button type="submit" disabled={false}>
              Submit
            </button>
          </Form>
        </>
      );
    };

  isEmpty = (value, falseStrings = [""]) =>
    value instanceof File
      ? false
      : typeof value === "undefined" ||
        value === null ||
        value !== value ||
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0) ||
        (typeof value === "string" && falseStrings.includes(value));

  formValidate = (values) => {
    let errors = {};

    if (this.isEmpty(values.party_name)) {
      errors.party_name = "Party Name is required";
    }
    if (this.isEmpty(values.amount)) {
      errors.amount = "Amount is required";
    }
    if (this.checkNumber(values.amount)) {
      errors.contact = "Amount must be a number";
    }
    return errors;
  };
  toggle = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  };

  handleSubmit = async (values) => {
    const newDataEntry = {
      partyName: values.party_name.value,
      partyCode: values.party_code.value,
      amount: values.amount,
    };
    this.props.addDataEntryDetails(newDataEntry);
    let response = await axios.get(`/api/party/${values.party_name.value}`);
    let balance =
      Number(response?.data[0]?.balance ?? 0) + Number(values.amount);
    await axios.post(`/api/party/update/${values.party_name.value}`, {
      balance: balance,
    });
    this.toggle();
    window.location.reload();
  };

  render() {
    return (
      <div>
        <Button
          color="dark"
          style={{ marginBottom: "2rem" }}
          onClick={this.toggle}
        >
          Add New Data Entry
        </Button>
        <Modal
          style={{ width: "600px" }}
          isOpen={this.state.isModalOpen}
          toggle={this.toggle}
        >
          <ModalHeader toggle={this.toggle}>Add Entry</ModalHeader>
          <ModalBody>
            <Formik
              validate={this.formValidate}
              initialValues={this.state.formInitialValues}
              render={this.renderView(this.props.partyDetail)}
              onSubmit={this.handleSubmit}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    partyDetail: state.partyDetail,
    dataEntryDetail: state.dataEntryDetail,
  };
};
export default connect(mapStateToProps, {
  getUsers,
  getPartyDetails,
  addDataEntryDetails,
  getDataEntryDetails,
  updatePartyDetails,
})(PartyModal);
