class Auth {
  constructor() {
    this.authenticated = false;
  }
  login(userData, cb) {
    localStorage.setItem("userData", userData);
    cb();
  }
  logout() {
    localStorage.removeItem("userData");
  }
  isAuthenticated() {
    let loginData = localStorage.getItem("userData");
    console.log("loginData", loginData);
    return !!loginData;
  }
}

export default new Auth();
