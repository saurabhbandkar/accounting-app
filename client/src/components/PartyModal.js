import React, { Component } from "react";
import { Formik, Form } from "formik";
import TextInput from "./common/TextInput";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { connect } from "react-redux";
import { addPartyDetails, getPartyDetails } from "../actions/partyActions";
import { getUsers } from "../actions/userActions";
// import { v4 as uuid } from "uuid";
import DropDown from "./common/Dropdown";

class PartyModal extends Component {
  state = {
    isModalOpen: false,
    formData: null,
    isLoading: false,
    isError: false,
    partyDetail: "",
  };
  componentDidMount() {
    this.props.getPartyDetails();
  }
  checkNumber = (number) => {
    let isNumber = false;
    if (number) {
      isNumber = isNaN(number) || parseInt(number) === 0;
    }
    return isNumber;
  };

  renderView =
    (partyDetail) =>
    ({ values, handleSubmit, setFieldValue, setFieldTouched }) => {
      const partyArray = partyDetail.party.map((partyData) => {
        return {
          value: partyData._id,
          label: partyData.partyName,
        };
      });
      if (this.state.isError) {
        return <p>Something went wrong</p>;
      }
      return (
        <>
          <Form>
            <TextInput
              labelTitle={"Enter Party Name"}
              labelFor={"party_name"}
              labelName={"party_name"}
              isMandatory
              placeholder={"Enter Party Name"}
            />
            <TextInput
              labelTitle={"Enter Party Code"}
              labelFor={"party_code"}
              labelName={"party_code"}
              isMandatory
              placeholder={"Enter Party Code"}
            />
            <TextInput
              labelTitle={"Enter Party Contact"}
              labelFor={"contact"}
              labelName={"contact"}
              isMandatory
              placeholder={"Enter Party Contact"}
            />
            <TextInput
              labelTitle={"Enter Credit Control"}
              labelFor={"credit_control"}
              labelName={"credit_control"}
              isMandatory
              placeholder={"Enter Credit Control"}
            />
            <TextInput
              labelTitle={"Party Status"}
              labelFor={"status"}
              labelName={"status"}
              isMandatory
              placeholder={"Party Status"}
            />
            <TextInput
              labelTitle={"Show in JV Master"}
              labelFor={"show_in_jv_master"}
              labelName={"show_in_jv_master"}
              isMandatory
              placeholder={"Show in JV Master"}
            />
            <TextInput
              labelTitle={"Enter Session Commission"}
              labelFor={"session_commission"}
              labelName={"session_commission"}
              isMandatory
              placeholder={"Enter Session Commission"}
            />
            <DropDown
              labelTitle={"Reference Type"}
              labelFor={"reference_type"}
              labelName={"reference_type"}
              option={[
                {
                  label: "New",
                  value: "yes",
                },
                {
                  label: "Existing",
                  value: "no",
                },
              ]}
            />
            {values.reference_type.value === "no" ? (
              <DropDown
                labelTitle={"Enter Reference"}
                labelFor={"reference"}
                labelName={"reference"}
                placeholder={"Enter Reference"}
                option={partyArray}
              />
            ) : (
              <TextInput
                labelTitle={"Enter Reference"}
                labelFor={"reference"}
                labelName={"reference"}
                isMandatory
              />
            )}
            <TextInput
              labelTitle={"Enter Session Partnership"}
              labelFor={"session_partnership"}
              labelName={"session_partnership"}
              isMandatory
              placeholder={"Enter Session Partnership"}
            />
            <TextInput
              labelTitle={"Enter Match Commission"}
              labelFor={"match_commission"}
              labelName={"match_commission"}
              isMandatory
              placeholder={"Enter Match Commission"}
            />
            <TextInput
              labelTitle={"Enter Match Partnership"}
              labelFor={"match_partnership"}
              labelName={"match_partnership"}
              isMandatory
              placeholder={"Enter Match Partnership"}
            />
            <TextInput
              labelTitle={"Enter Khancha Commission"}
              labelFor={"khancha_commission"}
              labelName={"khancha_commission"}
              isMandatory
              placeholder={"Enter Khancha Commission"}
            />
            <TextInput
              labelTitle={"Enter Khancha Partnership"}
              labelFor={"khancha_partnership"}
              labelName={"khancha_partnership"}
              isMandatory
              placeholder={"Enter Khancha Partnership"}
            />
            <button type="submit" disabled={false}>
              Submit
            </button>
          </Form>
        </>
      );
    };
  isEmpty = (value, falseStrings = [""]) =>
    value instanceof File
      ? false
      : typeof value === "undefined" ||
        value === null ||
        value !== value ||
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0) ||
        (typeof value === "string" && falseStrings.includes(value));

  formInitialValues = {
    party_name: "",
    party_code: "",
    contact: "",
    credit_control: "",
    status: "",
    show_in_jv_master: "",
    session_commission: "",
    session_partnership: "",
    match_commission: "",
    match_partnership: "",
    khancha_commission: "",
    khancha_partnership: "",
    reference_type: "yes",
  };

  formValidate = (values) => {
    let errors = {};

    if (this.isEmpty(values.party_code)) {
      errors.party_code = "Party Code is required";
    }
    if (this.isEmpty(values.contact)) {
      errors.contact = "Contact is required";
    }
    if (this.checkNumber(values.contact)) {
      errors.contact = "Contact must be a number";
    }
    if (this.isEmpty(values.credit_control)) {
      errors.credit_control = "Credit Control is required";
    }
    if (this.checkNumber(values.credit_control)) {
      errors.credit_control = "Credit Control must be a number";
    }
    if (this.isEmpty(values.status)) {
      errors.status = "Status is required";
    }
    if (this.isEmpty(values.reference)) {
      errors.reference = "Reference is required";
    }
    if (this.isEmpty(values.show_in_jv_master)) {
      errors.show_in_jv_master = "Show in JV master option is required";
    }
    if (this.isEmpty(values.session_commission)) {
      errors.session_commission = "Session Commission is required";
    }
    if (this.checkNumber(values.session_commission)) {
      errors.session_commission = "Session Commission must be a number";
    }
    if (this.isEmpty(values.session_partnership)) {
      errors.session_partnership = "Session Partnership is required";
    }
    if (this.checkNumber(values.session_partnership)) {
      errors.session_partnership = "Session Partnership must be a number";
    }
    if (this.isEmpty(values.match_commission)) {
      errors.match_commission = "Match Commission is required";
    }
    if (this.checkNumber(values.match_commission)) {
      errors.match_commission = "Match Commission must be a number";
    }
    if (this.isEmpty(values.match_partnership)) {
      errors.match_partnership = "Match Partnership is required";
    }
    if (this.checkNumber(values.match_partnership)) {
      errors.match_partnership = "Match Partnership must be a number";
    }
    if (this.isEmpty(values.khancha_commission)) {
      errors.khancha_commission = "Khancha Commission is required";
    }
    if (this.checkNumber(values.khancha_commission)) {
      errors.khancha_commission = "Khancha Commission must be a number";
    }
    if (this.isEmpty(values.khancha_partnership)) {
      errors.khancha_partnership = "Khancha Partnership is required";
    }
    if (this.checkNumber(values.khancha_partnership)) {
      errors.khancha_partnership = "Khancha Partnership must be a number";
    }
    return errors;
  };
  toggle = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  };

  handleSubmit = (values) => {
    const newPartyDetail = {
      partyName: values.party_name,
      partyCode: values.party_code,
      contact: values.contact,
      creditControl: values.credit_control,
      reference:
        values.reference_type.value === "Existing"
          ? values.reference.value
          : values.reference,
      status: values.status,
      showInJVMaster: values.show_in_jv_master,
      sessionCommission: values.session_commission,
      sessionPartnership: values.session_partnership,
      matchCommission: values.match_commission,
      matchPartnership: values.match_partnership,
      khanchaCommission: values.khancha_commission,
      khanchaPartnership: values.khancha_partnership,
    };
    this.props.addPartyDetails(newPartyDetail);
    this.toggle();
    window.location.reload();
  };

  render() {
    return (
      <div>
        <Button
          color="dark"
          style={{ marginBottom: "2rem" }}
          onClick={this.toggle}
        >
          Add New Party
        </Button>
        <Modal
          style={{ width: "600px" }}
          isOpen={this.state.isModalOpen}
          toggle={this.toggle}
        >
          <ModalHeader toggle={this.toggle}>Add Party Details</ModalHeader>
          <ModalBody>
            <Formik
              validate={this.formValidate}
              initialValues={this.formInitialValues}
              render={this.renderView(this.props.partyDetail)}
              onSubmit={this.handleSubmit}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    partyDetail: state.partyDetail,
  };
};
export default connect(mapStateToProps, {
  getUsers,
  getPartyDetails,
  addPartyDetails,
})(PartyModal);
