import React, { Component } from "react";
import { Formik, Form } from "formik";
import TextInput from "./common/TextInput";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import { connect } from "react-redux";
import { addTestDetails } from "../actions/testActions";
import { getUsers } from "../actions/userActions";
// import { v4 as uuid } from "uuid";
import Dropdown from "./common/Dropdown";

class TestingModal extends Component {
  state = {
    isModalOpen: false,
    userid: "",
    totalMarks: "",
    marksObtained: "",
    testName: "",
    formData: null,
    isLoading: false,
    isError: false,
    userData: "",
  };
  componentDidMount() {
    this.props.getUsers();
  }
  checkNumber = (number) => {
    let isNumber = false;
    if (number) {
      isNumber = isNaN(number) || parseInt(number) === 0;
    }
    return isNumber;
  };

  renderView =
    (userData) =>
    ({ values, handleSubmit, setFieldValue, setFieldTouched }) => {
      const userArray = userData.users.map((user) => {
        return {
          value: user._id,
          label: user.name,
        };
      });
      if (this.state.isError) {
        return <p>Something went wrong</p>;
      }
      return (
        <>
          <Form>
            <Dropdown
              disabled={false}
              labelTitle={"Select a candidate"}
              labelFor={"userName"}
              labelName={"userName"}
              isMandatory
              option={userArray}
              split
              placeholder={"Select a candidate"}
              isClearable
              noOptionsMessageText={"No users found"}
            />
            <TextInput
              split
              labelTitle="Enter test name"
              labelFor="testName"
              labelName="testName"
              isMandatory
            />
            <TextInput
              split
              labelTitle="Enter marks obtained"
              labelFor="marksObtained"
              labelName="marksObtained"
              isMandatory
            />
            <TextInput
              split
              labelTitle="Enter total marks"
              labelFor="totalMarks"
              labelName="totalMarks"
              isMandatory
            />
            <button type="submit" disabled={false}>
              Submit
            </button>
          </Form>
        </>
      );
    };
  isEmpty = (value, falseStrings = [""]) =>
    value instanceof File
      ? false
      : typeof value === "undefined" ||
        value === null ||
        value !== value ||
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0) ||
        (typeof value === "string" && falseStrings.includes(value));

  formInitialValues = {
    userName: "",
    totalMarks: "",
    marksObtained: "",
    testName: "",
  };

  formValidate = (values) => {
    let errors = {};

    if (this.isEmpty(values.userName)) {
      errors.userName = "User name is required";
    }
    if (this.isEmpty(values.marksObtained)) {
      errors.marksObtained = "Test marks obtained is required";
    }
    if (this.isEmpty(values.totalMarks)) {
      errors.totalMarks = "Test total marks is required";
    }
    if (values.totalMarks < 0) {
      errors.totalMarks = "Test total marks should be positive";
    }
    if (values.marksObtained < 0) {
      errors.marksObtained = "Test marks obtained should be positive";
    }
    if (values.marksObtained > values.totalMarks) {
      errors.totalMarks = "Test Total marks cannot be less than obtained marks";
    }
    if (this.isEmpty(values.testName)) {
      errors.testName = "Test name is required";
    }

    return errors;
  };
  toggle = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  };

  handleSubmit = (values) => {
    const newTestDetail = {
      testName: values.testName,
      totalMarks: values.totalMarks,
      marksObtained: values.marksObtained,
      userid: values.userName.value,
    };
    this.props.addTestDetails(newTestDetail);
    this.toggle();
  };

  render() {
    const users = this.props.user;
    return (
      <div>
        <Button
          color="dark"
          style={{ marginBottom: "2rem" }}
          onClick={this.toggle}
        >
          Add New Test
        </Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Add Test Details</ModalHeader>
          <ModalBody>
            <Formik
              validate={this.formValidate}
              initialValues={this.formInitialValues}
              render={this.renderView(users)}
              onSubmit={this.handleSubmit}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  testDetail: state.testDetail,
  user: state.user,
});
export default connect(mapStateToProps, { getUsers, addTestDetails })(
  TestingModal
);
