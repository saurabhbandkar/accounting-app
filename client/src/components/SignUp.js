import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import TextInput from "./common/TextInput";
import OTPverification from "./OTPverification";
import axios from "axios";
import Loader from "./common/Loader";
import ErrorModal from "./common/ErrorModal";

function SignUp() {
  function checkNumber(number) {
    let isNumber = false;
    if (number) {
      isNumber = isNaN(number) || parseInt(number) === 0;
    }
    return isNumber;
  }

  const isEmpty = (value, falseStrings = [""]) =>
    value instanceof File
      ? false
      : typeof value === "undefined" ||
        value === null ||
        value !== value ||
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0) ||
        (typeof value === "string" && falseStrings.includes(value));

  const formInitialValues = {
    email: "",
    password: "",
    name: "",
    contact: "",
  };
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [formData, setformData] = useState({});
  const [isError, setIsError] = useState(false);
  const [isEmailDuplicate, setIsEmailDuplicate] = useState(false);
  function formValidate(values) {
    let errors = {};
    if (isEmpty(values.name)) {
      errors.name = "User name is required";
    }
    if (isEmpty(values.email)) {
      errors.email = "User email is required";
    }
    if (isEmpty(values.contact)) {
      errors.contact = "User contact is required";
    }
    if (values.contact.length > 10) {
      errors.contact = "contact no. cannot be more than 10 digits";
    }
    if (isEmpty(values.password)) {
      errors.password = "User password is required";
    }
    if (checkNumber(values.contact)) {
      errors.contact = "Enter valid contact number";
    }

    return errors;
  }

  const handleSubmit = async (values) => {
    setIsLoading(true);
    setformData(values);
    try {
      const response = await axios.post("api/signup", values);
      if (response?.status === 200) {
        setIsLoading(false);
        setIsFormSubmitted(true);
      }
    } catch (error) {
      setIsLoading(false);
      setIsEmailDuplicate(true);
    }
  };
  const toggle = () => {
    setIsEmailDuplicate(false);
  };
  const renderView = ({
    values,
    handleSubmit,
    setFieldValue,
    setFieldTouched,
  }) => {
    if (isError) {
      return <p>Something went wrong</p>;
    }
    const {
      email = null,
      password = null,
      name = null,
      contact = null,
    } = values;
    return (
      <>
        <Form>
          <TextInput
            split
            labelTitle="name"
            labelFor="name"
            labelName="name"
            isMandatory
            placeholder="Sherlock Holmes"
          />
          <TextInput
            split
            labelTitle="contact"
            labelFor="contact"
            labelName="contact"
            isMandatory
            placeholder="9123456789"
          />
          <TextInput
            split
            labelTitle="email"
            labelFor="email"
            labelName="email"
            isMandatory
            placeholder="sherlock@gmail.com"
            notes={isEmailDuplicate && "Email already exists"}
          />
          <TextInput
            split
            labelTitle="password"
            labelFor="password"
            labelName="password"
            isMandatory
            type="password"
          />

          <button type="submit" disabled={false}>
            Submit
          </button>
        </Form>
      </>
    );
  };
  return (
    <>
      {isLoading && <Loader />}
      <ErrorModal
        isModalOpen={isEmailDuplicate}
        errorMessage={"Email ID already exists"}
        toggleModal={toggle}
      />
      {!isFormSubmitted && (
        <div>
          <h1>Enter Details</h1>
          <Formik
            validate={formValidate}
            initialValues={formInitialValues}
            render={renderView}
            onSubmit={handleSubmit}
          />
        </div>
      )}
      {isFormSubmitted && <OTPverification userEmail={formData.email} />}
    </>
  );
}

export default SignUp;
