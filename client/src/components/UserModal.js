import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { connect } from "react-redux";
import { addUser } from "../actions/userActions";
import { v4 as uuid } from "uuid";

class UserModal extends Component {
  state = {
    isModalOpen: false,
    name: "",
    email: "",
    contact: "",
    role: "",
    hasSubscribed: false,
  };
  toggle = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  };
  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  onSubmit = (event) => {
    event.preventDefault();
    const newUser = {
      name: this.state.name,
      email: this.state.mail,
      contact: this.state.contact,
      role: this.state.role,
      hasSubscribed: this.state.hasSubscribed,
    };
    this.props.addUser(newUser);
    this.toggle();
  };

  render() {
    return (
      <div>
        <Button
          color="dark"
          style={{ marginBottom: "2rem" }}
          onClick={this.toggle}
        >
          Add item
        </Button>
        <Modal isOpen={this.state.isModalOpen} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Add users</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for="user">User</Label>
                <Input
                  type="text"
                  name="name"
                  id="user"
                  placeholder="Add user"
                  onChange={this.onChange}
                />
                <Label for="mail">Email id</Label>
                <Input
                  type="email"
                  name="mail"
                  id="mail"
                  placeholder="Add mail id"
                  onChange={this.onChange}
                />
                <Label for="contact">Contact</Label>
                <Input
                  type="number"
                  name="contact"
                  id="contact"
                  placeholder="Add contact details"
                  onChange={this.onChange}
                />
                <Label for="role">Role</Label>
                <Input
                  type="text"
                  name="role"
                  id="role"
                  placeholder="Add user role"
                  onChange={this.onChange}
                />
                <Label for="hasSubscribed">Subscription status</Label>
                <Input
                  type="text"
                  name="hasSubscribed"
                  id="hasSubscribed"
                  placeholder="Add user subscription status"
                  onChange={this.onChange}
                />
                <Button color="dark" style={{ marginTop: "2rem" }} block>
                  Add User
                </Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user,
});
export default connect(mapStateToProps, { addUser })(UserModal);
