import React, { useState } from "react";
import { Formik, Form } from "formik";
import TextInput from "./common/TextInput";
import axios from "axios";
import Loader from "./common/Loader";
import ErrorModal from "./common/ErrorModal";
import SuccessModal from "./common/SuccessModal";

function OTPverification(props) {
  const email = props.userEmail;
  function checkNumber(number) {
    let isNumber = false;
    if (number) {
      isNumber = isNaN(number) || parseInt(number) === 0;
    }
    return isNumber;
  }

  const isEmpty = (value, falseStrings = [""]) =>
    value instanceof File
      ? false
      : typeof value === "undefined" ||
        value === null ||
        value !== value ||
        (typeof value === "object" && Object.keys(value).length === 0) ||
        (typeof value === "string" && value.trim().length === 0) ||
        (typeof value === "string" && falseStrings.includes(value));

  const formInitialValues = {
    OTP: "",
  };
  const [formData, setFormData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isOTPIncorrect, setIsOTPIncorrect] = useState(false);
  const [isUserAdded, setIsUserAdded] = useState(false);
  function formValidate(values) {
    let errors = {};
    if (isEmpty(values.OTP)) {
      errors.OTP = "OTP is required";
    }
    if (checkNumber(values.OTP)) {
      errors.OTP = "Enter OTP in number format only";
    }
    if (values.OTP.length > 6) {
      errors.OTP = "OTP shall be 6 digit only";
    }
    return errors;
  }

  const handleSubmit = async (values) => {
    setIsLoading(true);
    values["email"] = email;
    try {
      const response = await axios.post("api/verifyotp", values);
      if (response?.status === 200) {
        setIsOTPIncorrect(false);
        try {
          const success = await axios.post("api/verifyuser", values);
        } catch (error) {
          console.log("error", error);
        }
        try {
          const userDataResponse = await axios.post("api/verifieduser", values);
          const userData = userDataResponse?.data?.verifiedUser[0];
          const result = await axios.post("api/users", userData);
          if (result) {
            setIsUserAdded(true);
          }
        } catch (error) {
          console.log("error", error);
        }
      }
    } catch (error) {
      setIsOTPIncorrect(true);
    }
    setIsLoading(false);
  };
  const toggle = () => {
    setIsOTPIncorrect(false);
  };
  const toggleSuccessModal = () => {
    setIsUserAdded(false);
  };
  const renderView = ({
    values,
    handleSubmit,
    setFieldValue,
    setFieldTouched,
  }) => {
    if (isError) {
      return <p>Something went wrong</p>;
    }
    return (
      <>
        <Form>
          <TextInput
            split
            labelTitle="OTP"
            labelFor="OTP"
            labelName="OTP"
            isMandatory
            placeholder="000000"
          />
          <button type="submit" disabled={false}>
            Submit
          </button>
        </Form>
      </>
    );
  };
  return (
    <>
      {isLoading && <Loader />}
      <ErrorModal
        isModalOpen={isOTPIncorrect}
        errorMessage={"OTP is incorrect"}
        toggleModal={toggle}
      />
      <SuccessModal
        isModalOpen={isUserAdded}
        errorMessage={"User Added successfully, please login"}
        toggleModal={toggleSuccessModal}
      />
      <div>
        <h1>Enter OTP</h1>
        <Formik
          validate={formValidate}
          initialValues={formInitialValues}
          render={renderView}
          onSubmit={handleSubmit}
        />
      </div>
    </>
  );
}

export default OTPverification;
