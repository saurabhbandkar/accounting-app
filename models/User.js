const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const UserModel = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    contact: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      required: true,
    },
    hasSubscribed: {
      type: Boolean,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    date: {
      type: Date,
      default: Date.now,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = User = mongoose.model("user", UserModel);
