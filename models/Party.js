const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const PartyModel = new Schema(
  {
    userId: { type: Schema.Types.ObjectId, ref: "User" },
    partyName: {
      type: String,
      required: true,
    },
    partyCode: {
      type: String,
      required: true,
    },
    contact: {
      type: String,
      required: true,
    },
    creditControl: {
      type: Number,
      required: true,
    },
    reference: { type: String }, //{ type: Schema.Types.ObjectId, ref: "Party" },
    status: {
      type: String,
      required: true,
    },
    showInJVMaster: {
      type: String,
      required: true,
    },
    sessionCommission: {
      type: Number,
      required: true,
    },
    sessionPartnership: {
      type: Number,
      required: true,
    },
    matchCommission: {
      type: Number,
      required: true,
    },
    matchPartnership: {
      type: Number,
      required: true,
    },
    khanchaCommission: {
      type: Number,
      required: true,
    },
    khanchaPartnership: {
      type: Number,
      required: true,
    },
    balance: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = Party = mongoose.model("party", PartyModel);
