const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const DataEntryModel = new Schema(
  {
    userId: { type: Schema.Types.ObjectId, required: true, ref: "User" },
    partyCode: {
      type: Schema.Types.ObjectId,
      ref: "Party",
      default: null,
    },
    amount: {
      type: Number,
      required: true,
      default: 0,
    },
    date: {
      type: Date,
      required: true,
      default: Date.now,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = DataEntry = mongoose.model("dataEntry", DataEntryModel);
