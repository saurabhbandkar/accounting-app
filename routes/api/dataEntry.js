const express = require("express");
const router = express.Router();
const bcryptjs = require("bcryptjs");
const ObjectId = require("mongodb").ObjectId;

//dataEntry model
const DataEntry = require("../../models/DataEntry");

//@route GET api/dataEntries
//@desc Get all dataEntries
//@access Public

router.get("/", (req, res) => {
  DataEntry.find()
    .sort({ createdAt: -1 })
    .then((dataEntries) => res.json(dataEntries));
});

router.get("/:id", (req, res) => {
  DataEntry.find({ _id: ObjectId(req.params.id) })
    .sort({ createdAt: -1 })
    .then((dataEntries) => res.json(dataEntries));
});

//@route POST api/dataEntries
//@desc Create a dataEntry
//@access Public

router.post("/", async (req, res) => {
  const DataEntryDetails = new DataEntry({
    userId: req.body.partyName,
    partyCode: req.body.partyCode,
    amount: req.body.amount,
  });
  DataEntryDetails.save().then((dataEntry) => res.json(dataEntry));
});

//@route DELETE api/dataEntries/:id
//@desc Delete a dataEntry
//@access Public

router.delete("/:id", (req, res) => {
  DataEntry.findById(req.params.id)
    .then((dataEntry) => {
      dataEntry.remove().then(() => res.json({ success: true }));
    })
    .catch((err) => res.status(404).json({ success: false }));
});

//@route UPDATE api/dataEntries/update/:id
//@desc UPDATE a dataEntry
//@access Public

router.post("/update/:id", async (req, res) => {
  DataEntry.updateOne(
    { _id: req.params.id },
    {
      userId: req.body.user_id,
      partyCode: req.body.party_code,
      amount: req.body.amount,
    },
    { upsert: true }
  )
    .then((dataEntry) => res.json(dataEntry))
    .catch((err) =>
      res.status(404).json({ status: `failed with error: ${err}` })
    );
});

module.exports = router;
