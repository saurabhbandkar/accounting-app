const express = require("express");
const router = express.Router();
const bcryptjs = require("bcryptjs");
const env = process.env.NODE_ENV || "development";
const config = require("../../config/config")[env];
const sgMail = require("@sendgrid/mail");
// const MailVerification = require("../../models/MailVerification");
const User = require("../../models/User");

router.post("/", async (req, res) => {
  function createOTP() {
    let minm = 100000;
    let maxm = 999999;
    let OTP = Math.floor(Math.random() * (maxm - minm + 1)) + minm;
    return OTP;
  }

  // const registerOTP = (OTP, email, password, userName, contact) => {
  //   const mailVerification = new MailVerification({
  //     mail: email,
  //     password: password,
  //     OTP: OTP,
  //     name: userName,
  //     contact: contact,
  //     email: email,
  //   });
  //   mailVerification
  //     .save()
  //     .then(() => console.log("OTP is saved"))
  //     .catch((error) => {
  //       console.error("error -> signup -> registerOTP", error);
  //     });
  // };
  function sendMail(email, password, name, contact) {
    const OTP = createOTP();
    sgMail.setApiKey(config.API_KEY);
    const msg = {
      to: email,
      from: "slplatformhelpdesk@gmail.com",
      subject: "Login verification for Sales Learning Platform",
      text: "Successfully integrated sendgrid mail.",
      html: `OTP for verifying your email address is <strong>${OTP}</strong>`,
    };
    sgMail
      .send(msg)
      .then(async () => {
        await registerOTP(OTP, email, password, name, contact);
        res.status(200).send({
          msg: "Email sent successfully",
        });
      })
      .catch((error) => {
        console.error("error -> signup -> sendMail", error);
      });
  }

  let { email, password, name, contact } = req.body;
  const saltRounds = 10;
  password = await bcryptjs.hash(password, saltRounds);

  async function checkMail(email) {
    const response = await User.find({ email: email });
    return response;
  }

  const checkExistingMail = await checkMail(email);
  if (checkExistingMail.length > 0) {
    res.status(406).send({
      msg: "User email is already registered with our portal",
    });
  } else {
    sendMail(email, password, name, contact);
  }
});

module.exports = router;
