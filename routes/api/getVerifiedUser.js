const express = require("express");
const router = express.Router();
// const MailVerification = require("../../models/MailVerification");

router.post("/", async (req, res) => {
  try {
    let { email, OTP } = req.body;

    // async function checkVerifiedUser(email) {
    //   const response = await MailVerification.find({
    //     email: email,
    //     isVerified: true,
    //     OTP: OTP,
    //   });
    //   return response;
    // }

    const verifiedUser = await checkVerifiedUser(email);
    if (verifiedUser.length > 0) {
      res.status(200).send({
        verifiedUser,
      });
    } else {
      throw new Error("User does not exist, unauthorized request");
    }
  } catch (error) {
    if (error.message === "User does not exist, unauthorized request") {
      return res.status(401).send({
        msg: error.message,
      });
    }
    return res.status(500).send({
      msg: error.message,
    });
  }
});

module.exports = router;
