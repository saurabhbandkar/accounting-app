const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const bcryptjs = require("bcryptjs");
const env = process.env.NODE_ENV || "development";
const config = require("../../config/config")[env];
const jwt = require("jsonwebtoken");

router.post("/", async (req, res) => {
  let { password, email } = req.body;
  async function findMail(email) {
    const response = await User.find({ email: email });
    return response;
  }

  const checkPassword = await findMail(email);
  console.log("checkPassword", checkPassword);
  if (checkPassword.length > 0) {
    const isPasswordCorrect = await bcryptjs.compare(
      password,
      checkPassword[0].password
    );

    if (isPasswordCorrect) {
      const secretkey = config.TOKEN_SECRET;
      const refreshSecretKey = config.REFRESH_TOKEN_SECRET;
      const accessToken = jwt.sign({ email }, secretkey, { expiresIn: "2d" });
      const refreshToken = jwt.sign({ email }, refreshSecretKey);

      res.status(200).send({
        accessToken,
        refreshToken,
      });
    } else {
      res.status(401).send({
        msg: "Incorrect password, login failed",
      });
    }
  } else {
    res.status(401).send({
      msg: "Email is not registered, unauthorized request",
    });
  }
});

module.exports = router;
