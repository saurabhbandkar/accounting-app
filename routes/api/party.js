const express = require("express");
const router = express.Router();
const bcryptjs = require("bcryptjs");
const ObjectId = require("mongodb").ObjectId;

//party model
const Party = require("../../models/Party");

//@route GET api/party
//@desc Get all party
//@access Public

router.get("/", (req, res) => {
  Party.find({})
    .sort({ createdAt: -1 })
    .then((party) => res.json(party));
});

router.get("/:id", (req, res) => {
  Party.find({ _id: ObjectId(req.params.id) })
    .sort({ createdAt: -1 })
    .then((party) => res.json(party));
});

//@route POST api/party
//@desc Create a party
//@access Public

router.post("/", async (req, res) => {
  const newParty = new Party({
    partyName: req.body.partyName,
    partyCode: req.body.partyCode,
    contact: req.body.contact,
    creditControl: req.body.creditControl,
    reference: req.body.reference,
    status: req.body.status,
    showInJVMaster: req.body.showInJVMaster,
    sessionCommission: req.body.sessionCommission,
    matchCommission: req.body.matchCommission,
    matchPartnership: req.body.matchPartnership,
    sessionPartnership: req.body.sessionPartnership,
    khanchaPartnership: req.body.khanchaPartnership,
    khanchaCommission: req.body.khanchaCommission,
  });
  newParty.save().then((party) => res.json(party));
});

//@route DELETE api/party/:id
//@desc Delete a party
//@access Public

router.delete("/:id", (req, res) => {
  Party.findById(req.params.id)
    .then((party) => {
      party.remove().then(() => res.json({ success: true }));
    })
    .catch((err) => res.status(404).json({ success: false }));
});

router.post("/update/:id", async (req, res) => {
  Party.updateOne({ _id: ObjectId(req.params.id) }, req.body, { upsert: true })
    .then((party) => {
      res.json("success");
    })
    .catch((err) =>
      res.status(404).json({ status: `failed with error: ${err}` })
    );
});

module.exports = router;
