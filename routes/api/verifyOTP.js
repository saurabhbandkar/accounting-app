const express = require("express");
const router = express.Router();
// const MailVerification = require("../../models/MailVerification");

router.post("/", async (req, res) => {
  try {
    let { OTP, email } = req.body;

    // async function checkOTP(email) {
    //   const response = await MailVerification.find({ email: email }).sort({
    //     date: -1,
    //   });
    //   return response;
    // }

    const checkExistingMail = await checkOTP(email);
    if (checkExistingMail.length > 0) {
      if (checkExistingMail[0].OTP === OTP) {
        res.status(200).send({
          msg: "OTP is correct",
        });
      } else {
        throw new Error("OTP Incorrect, please try again");
      }
    } else {
      throw new Error("Please resend mail again, unauthorized request");
    }
  } catch (error) {
    if (error.message === "OTP Incorrect, please try again") {
      return res.status(400).send({
        msg: error.message,
      });
    } else if (
      error.message === "Please resend mail again, unauthorized request"
    ) {
      return res.status(401).send({
        msg: error.message,
      });
    }
    return res.status(500).send({
      msg: error.message,
    });
  }
});

module.exports = router;
