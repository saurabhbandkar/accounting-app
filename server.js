const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const users = require("./routes/api/users");
const party = require("./routes/api/party");
const dataEntry = require("./routes/api/dataEntry");
const signup = require("./routes/api/signup");
const verifyOTP = require("./routes/api/verifyOTP");
const login = require("./routes/api/login");
const getVerifiedUser = require("./routes/api/getVerifiedUser");
const setVerifiedUser = require("./routes/api/setVerifiedUser");
const env = process.env.NODE_ENV || "development";
const config = require("./config/config")[env];
const cors = require("cors");

const app = express();
app.use(cors());

//Boyparser middleware
app.use(bodyParser.json());

//DB config
const db = config.mongoURI;

//Connect to mongoDB
mongoose
  .connect(db)
  .then(() => {
    console.log("Mongo DB connected");
  })
  .catch((error) => console.log(error));

app.use("/api/users", users);
app.use("/api/party", party);
app.use("/api/dataentry", dataEntry);
app.use("/api/verifiedUser", getVerifiedUser);
app.use("/api/verifyUser", setVerifiedUser);
app.use("/api/signup", signup);
app.use("/api/verifyotp", verifyOTP);
app.use("/api/login", login);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
